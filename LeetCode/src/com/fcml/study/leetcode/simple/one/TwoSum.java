package com.fcml.study.leetcode.simple.one;

/**
 * @author duanwenkai
 * @date 2018/10/22
 */
public class TwoSum {

    private static int[] twoSum(int[] nums, int target) {
        int i, j;
        int[] result = new int[2];
        int length = nums.length;
        for (i = 0; i <= length - 2; i++) {
            for (j = i + 1; j <= length - 1; j++) {
                if (nums[i] + nums[j] == target) {
                    result[0] = i;
                    result[1] = j;
                }
            }
        }
        return result;
    }

    public static void main(String[] args) {
        int[] nums = {3, 2, 4};
        int target = 6;
        int result[] = twoSum(nums, target);
        System.out.println("[" + result[0] + " ," + result[1] + "]");
    }
}
