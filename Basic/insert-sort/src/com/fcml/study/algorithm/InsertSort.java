package com.fcml.study.algorithm;

public class InsertSort {
    public static void main(String[] args) {
        int[] arr = {15, 22, 56, 62, 4, 36, 51, 17, 29, 23};
        insertSort(arr);
        for (int num : arr) {
            System.out.print(num + ", ");
        }
    }


    private static void insertSort(int[] arr) {
        int length = arr.length;
        int temp;

        for (int i = 1; i < length; i++) {
            temp = arr[i];              //岗哨
            int j = i - 1;
            for (; j >= 0 && temp < arr[j]; --j) {
                arr[j + 1] = arr[j];    //大值后移
            }
            arr[j + 1] = temp;          //将岗哨copy到比较出的位置，完成一次插入
        }
    }
}
