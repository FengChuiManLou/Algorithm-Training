package com.fcml.study.algorithm;

public class SelectSort {

    public static void main(String[] args) {
        int[] arr = {15, 22, 56, 62, 4, 36, 51, 17, 29, 23};
        selectSort(arr);
        for (int num : arr) {
            System.out.print(num + ", ");
        }
    }
    // 9.8.5.1

    private static void selectSort(int[] arr) {
        for (int i = 0; i < arr.length - 1; i++) {
            int temp = i;           //记录每轮查询中最小值的位置
            for (int j = temp + 1; j < arr.length; j++) {
                if(arr[j] < arr[temp]){
                    temp = j;
                }
            }
        }
    }

}
