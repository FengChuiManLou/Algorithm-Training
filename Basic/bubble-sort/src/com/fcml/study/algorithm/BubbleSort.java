package com.fcml.study.algorithm;

public class BubbleSort {

    public static void main(String[] args) {
        int[] arr = {15, 22, 56, 62, 4, 36, 51, 17, 29, 23};
        bubbleSort(arr);
        for (int num : arr) {
            System.out.print(num + ", ");
        }
    }

    private static void bubbleSort(int[] arr) {
        for (int i = 0; i < arr.length - 1; i++) {
            for (int j = 0; j < arr.length - 1 - i; j++) {
                if (arr[j] > arr[j + 1]) {
                    int temp = arr[j];
                    arr[j] = arr[j + 1];
                    arr[j + 1] = temp;
                }
            }
        }
    }
}
